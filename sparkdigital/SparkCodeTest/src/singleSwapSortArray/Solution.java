package singleSwapSortArray;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {
	private boolean soultion(int[] a, int i, int j) {
		int left = positionateLeftIndex(a, i, j);
		if (left == j) {
			return true;
		}
		int right = positionateRightIndex(a, left, j);
		return isSorted(a, left, right);
	}

	private boolean isSorted(int[] a, int i, int j) {
		if (a[j] > a[i + 1] || a[i] < a[j - 1]) {
			return false;
		}
		int k = a.length - 1;
		swap(a, i,j);
		boolean is_Sorted = positionateLeftIndex(a, 0, k) == k;
		swap(a, j,i);
		return is_Sorted;
	}

	private void swap(int[] a, int i, int j) {
		int temp = a[j];
		a[j] = a[i];
		a[i] = temp;
	}

	private int positionateLeftIndex(int[] a, int i, int j) {
		int left = i;
		while (left < j && a[left] <= a[left + 1]) {
			++left;
		}
		return left;
	}
	
	private int positionateRightIndex(int[] a, int i, int j) {
		int right = j;
		while (right > i && a[right] >= a[right - 1] && a[i] <= a[right]) {
			--right;
		}
		return right;
	}
	
	private static void print(int[] arr, boolean solution) {
		System.out.println(Arrays.toString(arr) + " ==> " + (solution == true ? "Test Pass" : "Test Not Pass"));
	}
	
	public boolean solution(int[] a) {
		int n = a.length - 1;
		return (null != a && a.length > 0 && (n < 2 || soultion(a, 0, n)));
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the array size: ");
		int numOfElements = Integer.parseInt(scan.nextLine());
		int[] arr = new int[numOfElements];
		for (int i = 0; i < arr.length; i++) {
			System.out.print("Enter the element " + (i + 1) + " of " + arr.length + " : ");
			arr[i] = scan.nextInt();
		}
		scan.close();
		Solution s = new Solution();
		System.out.println("========================================================");
		System.out.println("The array " + Arrays.toString(arr) + (s.solution(arr) == true ? " can " : " can't ") + "be sorted");
		System.out.println("========================================================");
		System.out.println("<<<<<TESTING>>>>");
		arr = new int[] { 1, 5 };
		print(arr, s.solution(arr));
		arr = new int[] { 16, 5 };
		print(arr, s.solution(arr));
		arr = new int[] { 1, 5, 3, 3, 7 };
		print(arr, s.solution(arr));
		arr = new int[] { 1, 5, 3, 7 };
		print(arr, s.solution(arr));
		arr = new int[] { 5, 3, 3, 3, 5 };
		print(arr, s.solution(arr));
		arr = new int[] { 1, 5, 3, 4, 7 };
		print(arr, ! s.solution(arr));
		arr = new int[] { 9, 10, 11, 14, 10, 11, 12, 15, 16, 17 };
		print(arr, ! s.solution(arr));
		arr = new int[] { 1, 45, 12, 25, 34, 45, 6};
		print(arr, s.solution(arr));
		arr = new int[] { 1, 3, 5, 3, 4};
		print(arr, ! s.solution(arr));
		arr = new int[] { 1, 3, 5};
		print(arr, s.solution(arr));
		arr = new int[] { 5, 7, 3};
		print(arr, ! s.solution(arr));
	}
}
