package jobsity.challenge.tenpingbowling.calculation;

import jobsity.challenge.tenpingbowling.frame.FrameBuilder;
import jobsity.challenge.tenpingbowling.frame.FrameI;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FrameScoreCalculatorTest {

    private List<Integer> playerThrows;
    private FrameBuilder frameBuilder;
    private ScoreCalculator<FrameI> frameScoreCalculator;

    @BeforeEach
    void setUp() {
        playerThrows = new ArrayList<>();
        frameBuilder = new FrameBuilder(playerThrows);
        frameScoreCalculator = new FrameScoreCalculator();
    }

    @AfterEach
    void tearDown() {
        playerThrows = null;
        frameBuilder = null;
        frameScoreCalculator = null;
    }

    private FrameI createStrike(String ball1, String ball2) {
        FrameI frame = frameBuilder
                .addThrow("X")
                .build();
        playerThrows.add(Integer.parseInt(ball1));
        playerThrows.add(Integer.parseInt(ball2));
        return frame;
    }

    private FrameI createSpear(String ball1, String ball2, Integer extraBall) {
        FrameI frame = createOpen(ball1, ball2);
        playerThrows.add(extraBall);
        return frame;
    }

    private FrameI createOpen(String ball1, String ball2) {
        FrameI frame = frameBuilder
                .addThrow(ball1)
                .addThrow(ball2)
                .build();
        return frame;
    }

    @Nested
    @DisplayName("In open frame")
    class OpenFrameTest {

        @Test
        @DisplayName("Score is the sum of both throws")
        void scoreIsTheSumOfBothThrowsIfOpenFrame() {

            int score = frameScoreCalculator.calculateScore(createOpen("1","2"));

            assertEquals(3, score);
        }
    }

    @Nested
    @DisplayName("In spare frame")
    class SpareFrameTest {

        @Test
        @DisplayName("Score is 10 plus the next throw's points")
        void scoreIs10PlusTheNextThrowScoreIfSpear() {

            int score = frameScoreCalculator.calculateScore(createSpear("2", "8", 6));

            assertEquals(16, score);
        }
    }

    @Nested
    @DisplayName("In strike frame")
    class StrikeFrameTest {

        @Test
        @DisplayName("Score is 10 plus the sum of the next 2 throws")
        void scoreIs10PlusTheSumOfTheNext2ThrowsIfStrike() {

            int score = frameScoreCalculator.calculateScore(createStrike("7", "2"));

            assertEquals(19, score);
        }
    }
}