package jobsity.challenge.tenpingbowling.util;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public final class TestUtil {
    public static Path loadingFile(Class clazz, String fileName) {
        try {
            return Paths.get(clazz.getClassLoader()
                    .getResource(fileName).toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(String.format("Unable to Load the file %s%n%nEXCEPTION:\t", fileName), e);
        }
    }

    public static String normalizeLineEnds(String s) {
        return s.replace("\r\n", "\n").replace('\r', '\n');
    }

    public static String fileNameWithOutExt (String fileName) {
        return Optional.of(fileName.lastIndexOf(".")).filter(i-> i >= 0)
                .map(i-> fileName.substring(0, i)).orElse(fileName);
    }
}
