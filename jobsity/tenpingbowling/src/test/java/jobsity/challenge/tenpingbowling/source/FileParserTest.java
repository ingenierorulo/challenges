package jobsity.challenge.tenpingbowling.source;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FileParserTest {

    @Mock
    private BufferedReader bufferedReader;
    private FileParser parser;
    private static final List<String> expectedOut = Arrays.asList("David    2", "Robert    6");

    @BeforeEach
    void setUp() throws IOException {
        when(bufferedReader.lines()).thenReturn(expectedOut.stream());

        parser = new PlayerFileParser(bufferedReader);
    }

    @AfterEach
    void tearDown() throws IOException {
        bufferedReader.close();
        bufferedReader = null;
        parser.close();
        parser = null;
    }

    @Test
    @DisplayName("Stream all file lines and returns one temp player for each")
    void streamAllLinesAndReturnTempPlayerForEach() {
        final Stream<TempPlayer> parse = parser.parse(bufferedReader);
        final boolean wasModified = expectedOut.stream()
                .map(StringUtils::normalizeSpace)
                .map(s -> s.split(" "))
                .map(atts -> new TempPlayer(atts[0].trim(), atts[1].trim()))
                .collect(Collectors.toSet())
                .retainAll(parse.collect(Collectors.toSet()));
        assertFalse(wasModified);
    }
}