package jobsity.challenge.tenpingbowling.game;

import jobsity.challenge.tenpingbowling.calculation.FrameScoreCalculator;
import jobsity.challenge.tenpingbowling.calculation.PlayerScoreCalculator;
import jobsity.challenge.tenpingbowling.calculation.ScoreCalculator;
import jobsity.challenge.tenpingbowling.player.PlayerI;
import jobsity.challenge.tenpingbowling.util.TestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class FileGameSourceTest {

    private TenPinBowlingGameSource tenPinBowlingGameSource;
    private ScoreCalculator<PlayerI> playerScoreCalculator;
    private enum PlayerName{ John, Matthew, Phil }
    static final Map<String, Map<String, Integer>> playerFinalScores = new HashMap<>();
    static {
        final PlayerName[] names = PlayerName.values();
        playerFinalScores.put("allFouls", generateAllFoulsScores(names));
        playerFinalScores.put("allStrikes", generateAllStrikeScores(names));
        playerFinalScores.put("normalGame", generateNormalGameScores(names));
    }

    @BeforeEach
    void setUp() {
        playerScoreCalculator = new PlayerScoreCalculator(new FrameScoreCalculator());
    }

    @AfterEach
    void tearDown() {
        tenPinBowlingGameSource = null;
        playerScoreCalculator = null;
    }

    @TestFactory
    public Stream<DynamicTest> translateDynamicTestsFromStream() {
        final List<String> files = Arrays.asList("allFouls.txt", "allStrikes.txt", "normalGame.txt");

        return files.stream()
          .map(file ->
              DynamicTest.dynamicTest(String.format("Test %s game ", TestUtil.fileNameWithOutExt(file)), () -> {

                  final FileGameSource gameSource = new FileGameSource(TestUtil.loadingFile(this.getClass(),file).toFile());
                  final BowlingGameMatchI game = gameSource.createGame();
                  final Map<String, Integer> playersScores = game.getPlayers().stream()
                          .collect(Collectors.toMap(PlayerI::getName, playerScoreCalculator::calculateScore));
                  // assertions
                  playersScores.forEach((k,v) -> {
                      final Integer finalScore = getFinalScore(file, k);
                      assertNotNull(finalScore);
                      assertEquals(finalScore, v);
                  });
              })
        );
    }

    private Integer getFinalScore(String file, String player) {
        return playerFinalScores.getOrDefault(TestUtil.fileNameWithOutExt(file),null)
                .getOrDefault(player,null);
    }



    private static Map<String, Integer> generateAllFoulsScores(PlayerName[] playerNames) {
        return Arrays.stream(playerNames).collect(Collectors.toMap(Enum::toString, s -> new Integer(0)));
    }

    private static Map<String, Integer> generateAllStrikeScores(PlayerName[] playerNames) {
        return Arrays.stream(playerNames).collect(Collectors.toMap(Enum::toString, s -> new Integer(300)));
    }

    private static Map<String, Integer> generateNormalGameScores(PlayerName[] playerNames) {

        Map<String, Integer> map = new HashMap<>(playerNames.length);
        map.put(PlayerName.Phil.name(),130);
        map.put(PlayerName.John.name(),167);

        return map;
    }
}