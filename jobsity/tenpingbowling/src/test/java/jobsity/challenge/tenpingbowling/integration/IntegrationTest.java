package jobsity.challenge.tenpingbowling.integration;

import jobsity.challenge.tenpingbowling.TenPinBowlingApplication;
import jobsity.challenge.tenpingbowling.util.TestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrationTest {
    private ByteArrayOutputStream byteArrayOutputStream;
    private PrintStream outputStream;
    private static final Map<String, String> expectedOuts = new HashMap<>();
    static {
        expectedOuts.put("allFouls",
                "Frames\t\t 1      2      3      4      5      6      7      8      9      10   \n" +
                        "John\n" +
                        "Pinfalls\t 0  0   0  0   0  0   0  0   0  0   0  0   0  0   0  0   0  0   0  0      0      0\n" +
                        "Score\t\t   0    0    0    0    0    0    0    0    0    0    0    0\n");
        expectedOuts.put("allStrikes",
                "Frames\t\t 1      2      3      4      5      6      7      8      9      10   \n" +
                        "Matthew\n" +
                        "Pinfalls\t    X      X      X      X      X      X      X      X      X      X      X      X\n" +
                        "Score\t\t   30     60     90    120    150    180    210    240    270    300    300    300\n");
        expectedOuts.put("normalGame",
                "Frames\t\t 1      2      3      4      5      6      7      8      9      10   \n" +
                        "John\n" +
                        "Pinfalls\t    X   7  /   9  0      X   0  8   8  /   0  6      X      X      X      8      1\n" +
                        "Score\t\t   20     39     48     66     74     84     90    120    148    167    167    167\n" +
                        "Phil\n" +
                        "Pinfalls\t 3  /      X   7  2   6  /   6  /   3  5      X   7  /   4  4   0  3\n" +
                        "Score\t\t   20     39     48     64     77     85    105    119    127    130\n");
    }

    @BeforeEach
    void setUp() {
        byteArrayOutputStream = new ByteArrayOutputStream();
        outputStream = new PrintStream(byteArrayOutputStream);
        System.setOut(outputStream);
    }

    @AfterEach
    void tearDown() throws IOException {
        byteArrayOutputStream.close();
        outputStream.close();
        byteArrayOutputStream = null;
        outputStream = null;

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
    }

    private String getSystemResourcePath(String fileName) {
        return TestUtil.loadingFile(this.getClass(), fileName).toString();
    }

    private void assertOutputEqualsWithNormalizedLineEnds(String expected) {
        String output = TestUtil.normalizeLineEnds(new String(byteArrayOutputStream.toByteArray()));
        assertEquals(expected, output);
    }

    @TestFactory
    public Stream<DynamicTest> translateDynamicTestsFromStream() {
        final List<String> files = Arrays.asList("allFouls.txt", "allStrikes.txt", "normalGame.txt");

        return files.stream()
                .map(file ->
                        DynamicTest.dynamicTest(String.format("Test %s game ", TestUtil.fileNameWithOutExt(file)), () -> {
                            TenPinBowlingApplication.main(new String[]{getSystemResourcePath(file)});
                            assertOutputEqualsWithNormalizedLineEnds(expectedOuts.get(TestUtil.fileNameWithOutExt(file)));
                            resetOut();
                        })
                );
    }

    private void resetOut() {
        outputStream.flush();
        byteArrayOutputStream.reset();
    }
}
