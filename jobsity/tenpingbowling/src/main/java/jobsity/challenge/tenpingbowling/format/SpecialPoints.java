package jobsity.challenge.tenpingbowling.format;

import lombok.Getter;

@Getter
public enum SpecialPoints {
    STRIKE("X"),
    SPEAR("/"),
    FOUL("F")
    ;

    private String character;

    SpecialPoints(String character) {
        this.character = character;
    }

}
