package jobsity.challenge.tenpingbowling.format;

import jobsity.challenge.tenpingbowling.frame.SpecialDomainNumber;
import jobsity.challenge.tenpingbowling.game.BowlingGameMatchI;
import jobsity.challenge.tenpingbowling.player.PlayerI;

import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * {@inheritDoc}
 */
public final class BowlingGameFormatter implements FormatterI<BowlingGameMatchI> {

    private final FormatterI<PlayerI> playerFormatter;
    public BowlingGameFormatter(FormatterI<PlayerI> playerFormatter) {
        this.playerFormatter = playerFormatter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(BowlingGameMatchI game) {
        return String.format("%s%n%s", framesToString(), game.getPlayers().stream()
                .sorted(Comparator.comparing(PlayerI::getName))
                .map(playerFormatter::format)
                .collect(Collectors.joining("\n")));
    }

    private String framesToString() {
        return IntStream.rangeClosed(SpecialDomainNumber.ONE.getNumber(), SpecialDomainNumber.TEN.getNumber())
                .boxed()
                .map(frame -> String.format("%-5s", Integer.toString(frame)))
                .collect(Collectors.joining("  ","Frames\t\t ",""));
    }
}
