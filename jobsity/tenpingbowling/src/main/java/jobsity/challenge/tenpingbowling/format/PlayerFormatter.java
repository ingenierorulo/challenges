package jobsity.challenge.tenpingbowling.format;

import jobsity.challenge.tenpingbowling.calculation.ScoreCalculator;
import jobsity.challenge.tenpingbowling.frame.FrameI;
import jobsity.challenge.tenpingbowling.player.PlayerI;

import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
public final class PlayerFormatter implements FormatterI<PlayerI> {

    private final FormatterI<FrameI> frameFormatter;
    private final ScoreCalculator<FrameI> frameScoreCalculator;

    public PlayerFormatter(FormatterI<FrameI> frameFormatter, ScoreCalculator<FrameI> frameScoreCalculator) {
        this.frameFormatter = frameFormatter;
        this.frameScoreCalculator = frameScoreCalculator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(PlayerI playerI) {
        return String.format("%s%n%s%n%s", playerI.getName(), formatPinfall(playerI), scoreToString(playerI));
    }

    private String formatPinfall(PlayerI playerI) {
        return playerI.getFrames().stream()
                .map(frame -> String.format("%5s", frameFormatter.format(frame)))
                .collect(Collectors.joining("  ","Pinfalls\t",""));
    }

    private String scoreToString(PlayerI playerI) {
        AtomicInteger ai = new AtomicInteger();
        return String.join("   ", "Score\t\t",
                playerI.getFrames().stream()
                        .map(frameScoreCalculator::calculateScore)
                        .map(ai::addAndGet)
                        .collect(StringBuilder::new, this::formatAccumulatedScore, StringBuilder::append)
                        .toString()
                        .trim());
    }

    private void formatAccumulatedScore(StringBuilder x, Integer y) {
        if (y > 0) {
            x.append("  ");
        }
        x.append(String.format("%5s", y));
    }
}
