package jobsity.challenge.tenpingbowling.calculation;

import jobsity.challenge.tenpingbowling.frame.FrameI;
import jobsity.challenge.tenpingbowling.player.PlayerI;

public class PlayerScoreCalculator implements ScoreCalculator<PlayerI>{
    private final ScoreCalculator<FrameI> frameScoreCalculator;

    public PlayerScoreCalculator(ScoreCalculator<FrameI> frameScoreCalculator) {
        this.frameScoreCalculator = frameScoreCalculator;
    }

    @Override
    public int calculateScore(PlayerI player) {
        return player.getFrames().stream()
                .map(frameScoreCalculator::calculateScore)
                .mapToInt(Integer::intValue)
                .sum();
    }
}
