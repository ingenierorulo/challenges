package jobsity.challenge.tenpingbowling.frame;

import jobsity.challenge.tenpingbowling.factory.FactoryI;

import java.util.Arrays;

public class FrameFactory implements FactoryI<FrameBuilder, FrameI> {

    @Override
    public FrameI create(FrameBuilder frameBuilder) {
        final int[] throwsList = frameBuilder.getThrowsList();
        final int[] throwsCopy = Arrays.copyOf(throwsList, throwsList.length);
        if (frameBuilder.isBonusThrow()) {
            return new ExtraFrame(frameBuilder.getPlayerThrows(), throwsCopy[0]);
        }
        if (Arrays.stream(throwsCopy).sum() == SpecialDomainNumber.TEN.getNumber()) {
            if (frameBuilder.getNextThrow() == SpecialDomainNumber.ONE.getNumber()) {
                return new StrikeFrame(frameBuilder.getPlayerThrows());
            }
            return new SpareFrame(frameBuilder.getPlayerThrows(), throwsCopy[0], throwsCopy[1]);
        }
        return new OpenFrame(frameBuilder.getPlayerThrows(), throwsCopy[0], throwsCopy[1]);
    }
}
