package jobsity.challenge.tenpingbowling.player;

import jobsity.challenge.tenpingbowling.frame.FrameI;

import java.util.List;

public interface PlayerI {

    String getName();
    List<Integer> getThrows();
    List<FrameI> getFrames();
}
