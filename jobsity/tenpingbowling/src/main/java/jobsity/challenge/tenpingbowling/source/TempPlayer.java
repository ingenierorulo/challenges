package jobsity.challenge.tenpingbowling.source;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public final class TempPlayer {

    private final String playerName;
    private final String score;
}
