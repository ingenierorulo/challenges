package jobsity.challenge.tenpingbowling.calculation;

import jobsity.challenge.tenpingbowling.frame.FrameI;

public final class FrameScoreCalculator implements ScoreCalculator<FrameI> {

    @Override
    public int calculateScore(FrameI frame) {
        return frame.getInvolvedThrows().stream()
                .mapToInt(Integer::intValue)
                .sum();
    }
}
