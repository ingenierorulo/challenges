package jobsity.challenge.tenpingbowling.player;

import jobsity.challenge.tenpingbowling.frame.FrameBuilder;
import jobsity.challenge.tenpingbowling.frame.FrameI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * {@inheritDoc}
 */
public final class Player implements PlayerI {

    private final String name;
    private final List<Integer> throwsList;
    private final List<FrameI> frames;

    Player(String name, List<Integer> throwsList, List<FrameI> frames) {
        this.name = name;
        this.throwsList = throwsList;
        this.frames = frames;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Integer> getThrows() {
        return Collections.unmodifiableList(throwsList);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FrameI> getFrames() {
        return Collections.unmodifiableList(frames);
    }

    public static PlayerBuilder builder() {
        return new PlayerBuilder();
    }

    public static class PlayerBuilder {

        private String name = "";
        private List<FrameI> frames = new ArrayList<>();
        private List<Integer> throwsList = new ArrayList<>();

        PlayerBuilder() {
        }

        public PlayerBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PlayerBuilder frame(Consumer<FrameBuilder> frameBuilder) {
            FrameBuilder builder = new FrameBuilder(throwsList, frames.size() >= 10);
            frameBuilder.accept(builder);

            frames.add(builder.build());

            return this;
        }

        public PlayerI build() {
            return new Player(name, throwsList, frames);
        }

        public List<FrameI> getFrames() {
            return frames;
        }
    }
}
