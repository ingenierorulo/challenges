package jobsity.challenge.tenpingbowling.validation;

import jobsity.challenge.tenpingbowling.exception.FileParsingException;
import jobsity.challenge.tenpingbowling.game.BowlingGameMatchI;
import jobsity.challenge.tenpingbowling.player.PlayerI;

public class GameValidator implements Validator<BowlingGameMatchI> {
    @Override
    public void validate(BowlingGameMatchI bowlingGameMatchI) {
        if (bowlingGameMatchI.getPlayers().stream()
                .map(PlayerI::getFrames)
                .anyMatch(frameIS -> frameIS.size() < 10 || frameIS.size() > 12)) {
            throw new FileParsingException("The amount of frames must be greater than 9 and lower than 11");
        }
    }
}
