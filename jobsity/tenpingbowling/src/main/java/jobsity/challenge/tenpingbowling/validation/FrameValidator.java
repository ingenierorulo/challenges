package jobsity.challenge.tenpingbowling.validation;

import jobsity.challenge.tenpingbowling.frame.FrameBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public class FrameValidator implements Validator<FrameBuilder>{
    static final Map<Predicate<FrameBuilder>, String> frameValidationMessages = new HashMap<>();
    static {
        frameValidationMessages.put(fb -> fb.getNextThrow() == 0, "At least one throw required for frame");
        frameValidationMessages.put(fb -> fb.isBonusThrow() && fb.getNextThrow() > 1, "Bonus frame can only accept one throw");
        frameValidationMessages.put(fb -> fb.getTotalPoints() > 10, "Invalid points for frame, max 10");
    }

    @Override
    public void validate(FrameBuilder frameBuilder) {
        final Optional<Predicate<FrameBuilder>> first = frameValidationMessages.keySet().stream()
                .filter(frameBuilderPredicate -> frameBuilderPredicate.test(frameBuilder))
                .findFirst();
        if (first.isPresent()){
            throw new IllegalArgumentException(frameValidationMessages.get(first.get()));
        }
        //
    }
}
