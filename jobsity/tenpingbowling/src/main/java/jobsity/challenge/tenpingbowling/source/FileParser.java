package jobsity.challenge.tenpingbowling.source;

import java.io.BufferedReader;
import java.io.Closeable;
import java.util.stream.Stream;

public interface FileParser extends Closeable {
    Stream<TempPlayer> parse(BufferedReader bufferedReader);
}
