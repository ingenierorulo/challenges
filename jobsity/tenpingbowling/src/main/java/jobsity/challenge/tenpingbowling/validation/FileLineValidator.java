package jobsity.challenge.tenpingbowling.validation;

import jobsity.challenge.tenpingbowling.exception.FileParsingException;

public class FileLineValidator implements Validator<String[]> {
    @Override
    public void validate(String[] line) {
        if (line != null && line.length != 2) {
            throw new FileParsingException("Invalid file format. " +
                    "The format should be the player name followed by " +
                    "the score of the ball separated by either tabs or spaces.");
        }
    }
}
