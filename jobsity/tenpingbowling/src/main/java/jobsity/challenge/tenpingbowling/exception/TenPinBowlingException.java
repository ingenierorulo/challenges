package jobsity.challenge.tenpingbowling.exception;

public class TenPinBowlingException extends RuntimeException {

    public TenPinBowlingException(String message) {
        super(message);
    }

    public TenPinBowlingException(Throwable cause) {
        super(cause);
    }
}
