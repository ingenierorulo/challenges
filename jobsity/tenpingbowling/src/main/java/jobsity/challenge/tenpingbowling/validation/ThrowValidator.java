package jobsity.challenge.tenpingbowling.validation;

import jobsity.challenge.tenpingbowling.frame.FrameBuilder;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class ThrowValidator implements Validator<String> {
    private static final Integer MAX_THROWS = 2;
    private static final Pattern VALID_PIN_PATTERN = Pattern.compile("[0-9]|0[1-9]|10|[xXfF]");
    private final FrameBuilder frameBuilder;

    public ThrowValidator(FrameBuilder frameBuilder) {
        this.frameBuilder = frameBuilder;
    }

    @Override
    public void validate(String pins) {
        final Predicate<Integer> isEqualsThanMaxThrowsAllowed = MAX_THROWS::equals;
        if (isEqualsThanMaxThrowsAllowed.test(frameBuilder.getNextThrow())) {
            throw new IllegalArgumentException("Invalid frame, only 2 throws allowed at most.");
        }

        final Predicate<String> isInvalidPinCount = pins1 -> !VALID_PIN_PATTERN.matcher(pins1).matches();
        if (isInvalidPinCount.test(pins)) {
            throw new IllegalArgumentException("Invalid pin count, expected a number between 0 and 10, 'X' or 'F'");
        }
    }
}
