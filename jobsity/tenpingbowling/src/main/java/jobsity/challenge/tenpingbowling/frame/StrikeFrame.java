package jobsity.challenge.tenpingbowling.frame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class StrikeFrame extends Frame {

    StrikeFrame(List<Integer> throwList) {
        super(throwList, 1);

        throwList.add(SpecialDomainNumber.TEN.getNumber());
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public List<Integer> getInvolvedThrows() {
        List<Integer> selfThrows = new ArrayList<>(getThrowList());
        selfThrows.add(getBonusBall());
        selfThrows.add(getSecondBonusBall());
        return Collections.unmodifiableList(selfThrows);
    }
}
