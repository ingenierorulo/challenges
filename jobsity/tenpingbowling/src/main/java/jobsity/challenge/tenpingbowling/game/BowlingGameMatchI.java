package jobsity.challenge.tenpingbowling.game;

import jobsity.challenge.tenpingbowling.player.PlayerI;

import java.util.Optional;
import java.util.Set;

public interface BowlingGameMatchI {

    Set<PlayerI> getPlayers();

    default Optional<PlayerI> getPlayer(String playerName) {
        return getPlayers().stream()
                .filter(player -> player.getName().equals(playerName))
                .findFirst();
    }
}
