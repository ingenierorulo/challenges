package jobsity.challenge.tenpingbowling.exception;

public class FileParsingException extends TenPinBowlingException {

    public FileParsingException(String message) {
        super(message);
    }

    public FileParsingException(Throwable cause) {
        super(cause);
    }
}
