package jobsity.challenge.tenpingbowling.frame;

import java.util.List;

final class OpenFrame extends Frame {

    OpenFrame(List<Integer> throwList, int firstThrowScore, int secondThrowScore) {
        super(throwList, 2);
        throwList.add(firstThrowScore);
        throwList.add(secondThrowScore);
    }
}
