package jobsity.challenge.tenpingbowling;

import jobsity.challenge.tenpingbowling.calculation.FrameScoreCalculator;
import jobsity.challenge.tenpingbowling.calculation.ScoreCalculator;
import jobsity.challenge.tenpingbowling.format.BowlingGameFormatter;
import jobsity.challenge.tenpingbowling.format.FormatterI;
import jobsity.challenge.tenpingbowling.format.FrameFormatter;
import jobsity.challenge.tenpingbowling.format.PlayerFormatter;
import jobsity.challenge.tenpingbowling.frame.FrameI;
import jobsity.challenge.tenpingbowling.game.BowlingGameMatchI;
import jobsity.challenge.tenpingbowling.game.FileGameSource;
import jobsity.challenge.tenpingbowling.game.TenPinBowlingGameSource;
import jobsity.challenge.tenpingbowling.player.PlayerI;
import jobsity.challenge.tenpingbowling.validation.GameValidator;
import jobsity.challenge.tenpingbowling.validation.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TenPinBowlingApplication {

    private static final Logger logger = LogManager.getLogger(TenPinBowlingApplication.class);

    private final BowlingGameMatchI tenPinBowling;
    private final FormatterI<BowlingGameMatchI> tpGameFormatter;
    private final Validator gameValidator = new GameValidator();

    public TenPinBowlingApplication(BowlingGameMatchI tenPinBowling, FormatterI<BowlingGameMatchI> tpGameFormatter) {
        gameValidator.validate(tenPinBowling);
        this.tenPinBowling = tenPinBowling;
        this.tpGameFormatter = tpGameFormatter;
    }

    public void print() {
        System.out.println(tpGameFormatter.format(tenPinBowling));
    }

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            logger.error("Please run the command with filename, ie:\nmvn compile && mvn exec:java -Dexec.args=\"normalGame.txt\"");
            return;
        }
        Path gameInputPath = Paths.get(args[0]);
        if (Files.notExists(gameInputPath)) {
            logger.error("The bowling scores file '%s' is not reachable!",gameInputPath.toString());
            return;
        }

        TenPinBowlingGameSource gameSource = new FileGameSource(gameInputPath.toString());
        ScoreCalculator<FrameI> frameScoreCalculator = new FrameScoreCalculator();
        FormatterI<PlayerI> playerFormatter = new PlayerFormatter(new FrameFormatter(), frameScoreCalculator);
        FormatterI<BowlingGameMatchI> gameFormatter = new BowlingGameFormatter(playerFormatter);

        final BowlingGameMatchI game = gameSource.createGame();
        TenPinBowlingApplication app = new TenPinBowlingApplication(game, gameFormatter);
        app.print();
    }
}
