package jobsity.challenge.tenpingbowling.calculation;

public interface ScoreCalculator<T> {

    int calculateScore(T element);
}
