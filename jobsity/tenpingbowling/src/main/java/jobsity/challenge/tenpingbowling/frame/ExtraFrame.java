package jobsity.challenge.tenpingbowling.frame;

import java.util.Collections;
import java.util.List;

final class ExtraFrame extends Frame {

    ExtraFrame(List<Integer> throwList, int firstThrow) {
        super(throwList, 1);

        throwList.add(firstThrow);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Integer> getInvolvedThrows() {
        return Collections.emptyList();
    }
}
