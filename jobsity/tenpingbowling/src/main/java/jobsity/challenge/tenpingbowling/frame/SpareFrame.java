package jobsity.challenge.tenpingbowling.frame;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class SpareFrame extends Frame {

    SpareFrame(List<Integer> throwList, int firstThrowScore, int secondThrowScore) {
        super(throwList, 2);

        throwList.add(firstThrowScore);
        throwList.add(secondThrowScore);
    }

    @Override
    public List<Integer> getInvolvedThrows() {
        return Stream.concat(getThrowList().stream(),Stream.of(getBonusBall()))
                .collect(Collectors.toList());
    }
}
