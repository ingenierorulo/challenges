package jobsity.challenge.tenpingbowling.frame;

import jobsity.challenge.tenpingbowling.format.SpecialPoints;
import jobsity.challenge.tenpingbowling.validation.FrameValidator;
import jobsity.challenge.tenpingbowling.validation.ThrowValidator;
import jobsity.challenge.tenpingbowling.validation.Validator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class FrameBuilder {

    static final Map<String, Integer> parsePointsMap = new HashMap<>();
    static {
        parsePointsMap.put(SpecialPoints.STRIKE.getCharacter(), SpecialDomainNumber.TEN.getNumber());
        parsePointsMap.put(SpecialPoints.FOUL.getCharacter(), SpecialDomainNumber.ZERO.getNumber());
    }

    private static final Integer MAX_THROWS = 2;
    private final List<Integer> playerThrows;
    private final boolean bonusThrow;
    private final int[] throwsList = new int[MAX_THROWS];
    private int nextThrow = 0;

    private final Validator frameValidator = new FrameValidator();
    private final Validator throwValidator = new ThrowValidator(this);
    private final FrameFactory factory = new FrameFactory();

    public FrameBuilder(List<Integer> playerThrows) {
        this(playerThrows, false);
    }

    public FrameBuilder(List<Integer> playerThrows, boolean bonusThrow) {
        this.playerThrows = playerThrows;
        this.bonusThrow = bonusThrow;
    }

    public FrameI build() {
        frameValidator.validate(this);
        return factory.create(this);
    }

    public FrameBuilder addThrow(String pins) {
        throwValidator.validate(pins);
        throwsList[nextThrow++] = parsePointsMap.computeIfAbsent(pins, Integer::parseInt);
        return this;
    }

    public boolean isBonusThrow() {
        return bonusThrow;
    }

    public int getTotalPoints() {
        return Arrays.stream(throwsList).sum();
    }

    public int getNextThrow() {
        return nextThrow;
    }

    public List<Integer> getPlayerThrows() {
        return playerThrows;
    }

    public int[] getThrowsList() {
        return throwsList;
    }
}
