package jobsity.challenge.tenpingbowling.source;

import jobsity.challenge.tenpingbowling.validation.FileLineValidator;
import jobsity.challenge.tenpingbowling.validation.Validator;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.stream.Stream;

/**
 * {@inheritDoc}
 */
public final class PlayerFileParser implements FileParser {

    private final BufferedReader bufferedReader;
    private final Validator fileLineValidator = new FileLineValidator();

    public PlayerFileParser() {
        bufferedReader = null;
    }

    public PlayerFileParser(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    @Override
    public Stream<TempPlayer> parse(BufferedReader bufferedReader) {
        return bufferedReader.lines()
                .map(l -> l.split("[\t ]+"))
                .peek(fileLineValidator::validate)
                .map(row -> new TempPlayer(row[0], row[1]));
    }

    @Override
    public void close() throws IOException {
        bufferedReader.close();
    }
}
