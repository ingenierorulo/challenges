package jobsity.challenge.tenpingbowling.format;

import jobsity.challenge.tenpingbowling.frame.FrameI;
import jobsity.challenge.tenpingbowling.frame.SpecialDomainNumber;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
public final class FrameFormatter implements FormatterI<FrameI> {

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(FrameI frame) {
        List<Integer> framePoints = frame.getThrowList();
        int frameTotal = framePoints.stream()
                .mapToInt(Integer::intValue)
                .sum();

        if (frameTotal == SpecialDomainNumber.TEN.getNumber()) {
            return  (frame.getFrameSize() == SpecialDomainNumber.TWO.getNumber())
                    ? (framePoints.get(SpecialDomainNumber.ZERO.getNumber()) + "  " + SpecialPoints.SPEAR.getCharacter())
                    : "  " + SpecialPoints.STRIKE.getCharacter();
        }
        return famePointsToString(framePoints);
    }

    private String famePointsToString(List<Integer> framePoints) {
        return framePoints.stream()
                .map(Objects::toString)
                .collect(Collectors.joining("  "));
    }
}
