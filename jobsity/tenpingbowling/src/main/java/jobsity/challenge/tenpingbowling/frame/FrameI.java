package jobsity.challenge.tenpingbowling.frame;

import java.util.List;

public interface FrameI {

    List<Integer> getThrowList();
    int getFrameSize();
    List<Integer> getInvolvedThrows();
}
