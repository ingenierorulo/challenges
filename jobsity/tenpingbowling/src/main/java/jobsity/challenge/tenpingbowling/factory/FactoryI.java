package jobsity.challenge.tenpingbowling.factory;

public interface FactoryI<T,R> {

    R create(T t);
}
