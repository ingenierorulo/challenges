package jobsity.challenge.tenpingbowling.validation;

public interface Validator<T> {
    void validate(T t);
}
