package jobsity.challenge.tenpingbowling.format;

public interface FormatterI<T> {

    String format(T element);
}
