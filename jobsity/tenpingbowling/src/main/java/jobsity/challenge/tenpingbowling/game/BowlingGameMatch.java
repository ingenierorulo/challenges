package jobsity.challenge.tenpingbowling.game;

import jobsity.challenge.tenpingbowling.exception.FileParsingException;
import jobsity.challenge.tenpingbowling.frame.FrameBuilder;
import jobsity.challenge.tenpingbowling.player.Player;
import jobsity.challenge.tenpingbowling.player.PlayerI;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
public class BowlingGameMatch implements BowlingGameMatchI {

    private final Set<PlayerI> players;

    BowlingGameMatch(Set<PlayerI> players) {
        this.players = players;
    }

    @Override
    public Set<PlayerI> getPlayers() {
        return Collections.unmodifiableSet(players);
    }

    public static BowlingGameBuilder builder() {
        return new BowlingGameBuilder();
    }

    public static class BowlingGameBuilder {

        private final Map<String, Player.PlayerBuilder> players = new TreeMap<>();

        public BowlingGameBuilder playerFrame(String playerName, Consumer<FrameBuilder> frameBuilder) {
            if (!players.containsKey(playerName)) {
                if (players.values().stream().
                        anyMatch(playerBuilder -> playerBuilder.getFrames().size() != 1)) {
                    throw new FileParsingException(playerName + " can not be added in the middle of the match");
                }
            }
            players.compute(playerName, (key, value) ->
                    value != null ? value : Player.builder().name(playerName)
            ).frame(frameBuilder);

            return this;
        }

        public BowlingGameMatchI build() {
            Set<PlayerI> playerISet = players.values().stream()
                    .map(Player.PlayerBuilder::build)
                    .collect(Collectors.toSet());
            return new BowlingGameMatch(playerISet);
        }
    }
}
