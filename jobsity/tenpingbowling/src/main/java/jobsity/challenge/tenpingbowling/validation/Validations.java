package jobsity.challenge.tenpingbowling.validation;

import java.util.function.Predicate;

public class Validations {
    private static final Predicate<String[]> noFileArgsValidation = args -> args == null || args.length == 0;
    private static final Predicate<String[]> unreachableFileArgsValidation = args -> args == null || args.length == 0;
}
