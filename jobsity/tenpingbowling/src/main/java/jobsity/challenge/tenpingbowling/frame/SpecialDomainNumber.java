package jobsity.challenge.tenpingbowling.frame;

import lombok.Getter;

@Getter
public enum SpecialDomainNumber {
    ZERO(0),
    ONE(1),
    TWO(2),
    TEN(10)
    ;

    private final Integer number;

    SpecialDomainNumber(Integer number) {
        this.number = number;
    }
}
