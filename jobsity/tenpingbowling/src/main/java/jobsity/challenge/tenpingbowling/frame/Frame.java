package jobsity.challenge.tenpingbowling.frame;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Frame implements FrameI {

    final List<Integer> throwsList;
    final int startingThrow;
    final int frameSize;

    Frame(List<Integer> throwsList, int frameSize) {
        Objects.requireNonNull(throwsList);

        this.throwsList = throwsList;
        this.startingThrow = throwsList.size();
        this.frameSize = frameSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final List<Integer> getThrowList() {
        return Collections.unmodifiableList(throwsList.subList(startingThrow, startingThrow + frameSize));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int getFrameSize() {
        return frameSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Integer> getInvolvedThrows() {
        return getThrowList();
    }

    final int getBonusBall() {
        return throwsList.get(startingThrow + frameSize);
    }

    final int getSecondBonusBall() {
        return throwsList.get(startingThrow + frameSize + 1);
    }
}
