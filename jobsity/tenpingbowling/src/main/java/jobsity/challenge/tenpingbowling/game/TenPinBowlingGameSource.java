package jobsity.challenge.tenpingbowling.game;

public interface TenPinBowlingGameSource {

    BowlingGameMatchI createGame();
}
