package jobsity.challenge.tenpingbowling.game;

import jobsity.challenge.tenpingbowling.exception.FileParsingException;
import jobsity.challenge.tenpingbowling.frame.FrameBuilder;
import jobsity.challenge.tenpingbowling.frame.SpecialDomainNumber;
import jobsity.challenge.tenpingbowling.source.FileParser;
import jobsity.challenge.tenpingbowling.source.PlayerFileParser;
import jobsity.challenge.tenpingbowling.source.TempPlayer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class FileGameSource implements TenPinBowlingGameSource {

    private final File file;
    private FileParser fileParser = new PlayerFileParser();
    private final Predicate<FrameBuilder> isOpenFrame =
            fb -> !fb.isBonusThrow() && fb.getTotalPoints() < SpecialDomainNumber.TEN.getNumber();


    public FileGameSource(File file) {
        this.file = file;
    }

    public FileGameSource(String filePath) {
        this(new File(filePath));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BowlingGameMatchI createGame() {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(file.getPath()))) {
            final Stream<TempPlayer> parse = fileParser.parse(br);
            final Iterator<TempPlayer> parseIterator = parse.iterator();
            return readInput(parseIterator);
        } catch (IOException e) {
            throw new FileParsingException(e);
        }
    }

    private BowlingGameMatchI readInput(Iterator<TempPlayer> tempPlayerIterator) {
        final BowlingGameMatch.BowlingGameBuilder tenPingGame = BowlingGameMatch.builder();
        while (tempPlayerIterator.hasNext()) {
            final TempPlayer currentPlayer = tempPlayerIterator.next();
            tenPingGame.playerFrame(currentPlayer.getPlayerName(),
                    getFrameBuilderConsumer(tempPlayerIterator, currentPlayer));
        }
        return tenPingGame.build();
    }

    private Consumer<FrameBuilder> getFrameBuilderConsumer(Iterator<TempPlayer> tempPlayerIterator, TempPlayer currentPlayer) {
        return frameBuilder -> {
            frameBuilder.addThrow(currentPlayer.getScore());
            if (isOpenFrame.test(frameBuilder)) {
                readNextThrow(frameBuilder, tempPlayerIterator, currentPlayer.getPlayerName());
            }
        };
    }

    private void readNextThrow(FrameBuilder frameBuilder, Iterator<TempPlayer> tempPlayerIterator, String currentPlayer) {
        final TempPlayer nextTempPlayer = tempPlayerIterator.next();
        if (nextTempPlayer == null) {
            return;
        }
        if (!nextTempPlayer.getPlayerName().equals(currentPlayer)) {
            throw new FileParsingException("Invalid player " + nextTempPlayer.getPlayerName() + ", expected " + currentPlayer);
        }
        frameBuilder.addThrow(nextTempPlayer.getScore());
    }
}
