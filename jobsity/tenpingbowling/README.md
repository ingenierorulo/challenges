
## :bowling: Jobsity -- Ten Pin Bowling Challenge :bowling:

A java command line application to view and calculate the 10 pin bowling game scoreboards.

### Technology Stack

* Project is based on Java 8
* The project was built using [Apache Maven](https://maven.apache.org).
* Tests are run using [JUnit 5](https://junit.org/junit5/)
* Mocks are handled using [Mockito](https://site.mockito.org)
* Logs are handled using [apache log4j](https://log4j.apache.org) and the system output
* Utility libraries: 
    * [Apache Commons Collections](https://commons.apache.org/proper/commons-collections/)
    * [Apache Commons Lang](https://commons.apache.org/proper/commons-lang/download_lang.cgi)
    * [Project Lombok](https://projectlombok.org/)
* Project execution is handled by the [Exec Maven Plugin](https://www.mojohaus.org/exec-maven-plugin/)


## All mandatory features and bonus tasks were completed successfully.

### Mandatory Features
- [x] The program should run from the command-line and take a text file as input
- [x] The program should read the input text file and parse its content, which should have the
results for several players bowling 10 frames each, written according to these guidelines:
- [x] Each line represents a player and a chance with the subsequent number of pins
knocked down.
- [x] An 'F' indicates a foul on that chance and no pins knocked down (identical for
scoring to a roll of 0).
- [x] The rows are tab-separated.
- [x] The program should handle bad input like more than ten throws (i.e., no chance will
produce a negative number of knocked down pins or more than 10, etc), invalid score
value or incorrect format
- [x] The program should output the scoring for the associated game according to these
guidelines:
- [x] For each player, print their name on a separate line before printing that player's
pinfalls and score.
- [x] All values are tab-separated.
- [x] The output should calculate if a player scores a strike ('X'), a spare ('/') and allow
for extra chances in the tenth frame.
- [x] Your program should be able to handle all possible cases of a game both including a
game where all rolls are 0, all rolls are fouls (F) and a perfect game, where all rolls are
strikes
- [x] Unit test: Tests should cover at least the non-trivial classes and methods
- [x] Integration test: At least cover the three main cases: Sample input (2 players), perfect
score, zero score

**Bonus (Optional)**

- [x] Use Java 8 streams and lambdas
## Execution steps
In the project root run:

**Unix**

_Compile and run_
```bash
./mvnw clean compile exec:java -Dexec.args -Dexec.args=/path/to/scoresFile.txt
```
_Test Cases_
```bash
./mvnw clean test
```
**Windows**

_Compile and run_
```bash
mvnw clean compile exec:java -Dexec.args -Dexec.args=/path/to/scoresFile.txt
```
_Test Cases_
```bash
mvnw clean test
```