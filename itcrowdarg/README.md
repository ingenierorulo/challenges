# README #

This document describes the necessary steps to get the gwt-sortable-itemslist application up and running.

### What is this repository for? ###

####Quick summary####
- The functionality of this application is to show a sortable list of items.
- Each item in the list consists of a textual description up to 25 chars.
- Each item could be to Added throw a textbox my clicking on "ADD" button or just pressing [ENTER], Removed or Edited.
- The list is be sortable by the text.

####Version####
1.0

### How do I get set up? ###

####Summary of set up####
  The application is a RESTFul app builed with spring boot with an embebed tomcat server in the back-end and Resty GWT 
  on frontend. All data is saved on a postgres database. The app is located in two docker containers, one for the app
  itself and other to persist the data.
  No login is required to use it

####Configuration####

 * Dependencies:
  The following technologies are required in order to deploy the application:
* * docker
 Installation instructions for Ubuntu:
     https://docs.docker.com/engine/installation/linux/ubuntu/#install-using-the-repository
* * git
* * https://git-scm.com/
* * java 1.8

####Database configuration####
* * Database engine: postgres
* * Databae name: gwtsortableitemlist
* * Schema: gwtsil
* * User: gwtpostgres
* * Role: gwtpostgresrole

####Deployment instructions####
  Just a few steps are required to deploy in you local:
* From source code:
  1. Dowload the project from bitbucket
     $> git clone https://ingenierorulo@bitbucket.org/ingenierorulo/challenges.git challenges
  2. point your shell to the gwt-sortable-itemslist directory.
     $> cd challenges/gwt-sortable-itemslist
  3. build your project with gradle 3.5:
       Important!: to make sure that everithing is working as expectd, create the gradle wrapper structure and make the 
                   gradlew executable. (Downloading https://services.gradle.org/distributions/gradle-3.5-bin.zip)

                   $> gradle wrapper
                   $> chmod 775 gradlew

       $> ./gradlew build
      This will be generate an executable jar file in build/libs/  
      (gwt-sortable-itemslist-1.0.0.jar)

  4. $> docker-compose build
  5. $> docker-compose up
  5.1 $> alternatively you can use the start-docker.sh script instead of step 4 and 5
  6.  check your application on localhost:8080

### Contribution guidelines ###

* Other guidelines
  In order to stop the app just type [Ctrl] + C
  From now on the applicacion could be strat just typeing docker-compose up
  

### Who do I talk to? ###

* Repo owner or admin
  Luis Pouzo: luispouzo@gmail.com