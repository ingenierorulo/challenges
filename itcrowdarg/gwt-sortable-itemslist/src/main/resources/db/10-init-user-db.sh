#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	
	CREATE DATABASE gwtsortableitemlist OWNER gwtpostgres;
	GRANT ALL PRIVILEGES ON DATABASE gwtsortableitemlist TO gwtpostgres;
	REVOKE ALL ON DATABASE gwtsortableitemlist FROM PUBLIC;
	CREATE ROLE gwtpostgresrole;

	\c gwtsortableitemlist;

	CREATE SCHEMA gwtsil authorization gwtpostgres;
	GRANT ALL PRIVILEGES ON SCHEMA gwtsil TO gwtpostgres;
	GRANT ALL PRIVILEGES ON SCHEMA gwtsil TO postgres;
	GRANT CONNECT ON DATABASE gwtsortableitemlist TO gwtpostgresrole;
	GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA gwtsil TO gwtpostgresrole;
	GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA gwtsil TO gwtpostgresrole;
	ALTER DEFAULT PRIVILEGES IN SCHEMA gwtsil GRANT ALL ON SEQUENCES TO gwtpostgresrole;
	GRANT gwtpostgresrole TO gwtpostgres;
	ALTER DEFAULT PRIVILEGES IN SCHEMA gwtsil GRANT ALL ON TABLES TO gwtpostgres;

	\q
EOSQL
