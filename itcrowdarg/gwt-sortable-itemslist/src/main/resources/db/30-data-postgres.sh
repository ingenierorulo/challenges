#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL

	\c gwtsortableitemlist;

	DELETE FROM gwtsil.item;
	INSERT INTO gwtsil.item (text) VALUES
	  ('Hello this is a test'),
	  ('Silver Banshee');

	\q
EOSQL
