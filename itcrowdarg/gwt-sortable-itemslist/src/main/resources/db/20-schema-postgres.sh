#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL

	\c gwtsortableitemlist;

	CREATE TABLE IF NOT EXISTS gwtsil.item (
	  id SERIAL PRIMARY KEY,
	  text VARCHAR(25),
	  created_date TIMESTAMP DEFAULT current_timestamp,
	  last_modified_date TIMESTAMP
	);

	\q
EOSQL
