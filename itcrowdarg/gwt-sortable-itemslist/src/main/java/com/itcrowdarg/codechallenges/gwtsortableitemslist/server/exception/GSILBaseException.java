package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception;

public class GSILBaseException extends RuntimeException {
	
	/** Serial Version */
	private static final long serialVersionUID = 6123570732944853349L;
	
	private GSILErrors errorCode;
	
	public GSILBaseException(GSILErrors errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public GSILBaseException(String message, GSILErrors errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public GSILBaseException(Throwable cause, GSILErrors errorCode) {
		super(cause);
		this.errorCode = errorCode;
	}

	public GSILBaseException(String message, Throwable cause, GSILErrors errorCode) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	public GSILBaseException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace, GSILErrors errorCode) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.errorCode = errorCode;
	}

	public GSILErrors getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(GSILErrors errorCode) {
		this.errorCode = errorCode;
	}

}
