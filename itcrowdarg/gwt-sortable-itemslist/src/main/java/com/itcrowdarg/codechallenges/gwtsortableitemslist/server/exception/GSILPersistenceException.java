package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception;

public class GSILPersistenceException extends GSILBaseException{

	/** Serial Version */
	private static final long serialVersionUID = 1471896086220290097L;

	public GSILPersistenceException(GSILErrors errorCode) {
		super(errorCode);
	}

	public GSILPersistenceException(String message, GSILErrors errorCode) {
		super(message, errorCode);
	}

	public GSILPersistenceException(Throwable cause, GSILErrors errorCode) {
		super(cause, errorCode);
	}

	public GSILPersistenceException(String message, Throwable cause,
			GSILErrors errorCode) {
		super(message, cause, errorCode);
	}

	public GSILPersistenceException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace,
			GSILErrors errorCode) {
		super(message, cause, enableSuppression, writableStackTrace, errorCode);
	}

}
