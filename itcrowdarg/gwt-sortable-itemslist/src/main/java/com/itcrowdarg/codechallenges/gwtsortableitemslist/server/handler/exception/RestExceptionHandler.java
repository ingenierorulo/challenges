package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.handler.exception;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception.GSILBaseException;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception.GSILErrors;

/**
 * Called when an exception occurs during request processing. Transforms exception message into JSON format.
 */
@RestControllerAdvice()
public class RestExceptionHandler {

    private static final Logger logger = Logger.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(value = {Exception.class})
    //@ResponseBody
    public ResponseEntity<GSILResponseMessage> handleGenericException(Exception ex) {
        if (logger.isDebugEnabled()) {
            logger.debug("handling exception...");
        }
        GSILResponseMessage responseMessage = new GSILResponseMessage( GSILErrors.INTERNAL_ERROR.getCode(), GSILErrors.INTERNAL_ERROR.getMessage(), ex.getMessage(), false );
		
        return new ResponseEntity<GSILResponseMessage>(responseMessage, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(value = {GSILBaseException.class})
    //@ResponseBody
    public ResponseEntity<GSILResponseMessage> handleBaseException(GSILBaseException ex) {
        if (logger.isDebugEnabled()) {
            logger.debug("handling exception...");
        }
        
        GSILResponseMessage responseMessage = new GSILResponseMessage( ex.getErrorCode().getCode(), ex.getMessage(), ex.getErrorCode().getMessage(), false );
		
        return new ResponseEntity<>(responseMessage, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(value={ MethodArgumentNotValidException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
    //@ResponseBody
	public ResponseEntity<GSILResponseMessage> handleMethodArgumentNotValid( MethodArgumentNotValidException ex) {
    	logger.error(ex.getMessage(), ex);

    	GSILResponseMessage responseMessage = new GSILResponseMessage(ex.getBindingResult().getFieldErrors().get(0).getCode(), 
																	"Error in field " + ex.getBindingResult().getFieldErrors().get(0).getField(), 
																	ex.getBindingResult().getFieldErrors().get(0).getDefaultMessage(), 
																	false );
    	
    	
    	return new ResponseEntity<GSILResponseMessage>(responseMessage, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(value = { ConstraintViolationException.class })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<GSILResponseMessage> handleResourceNotFoundException(ConstraintViolationException e) {
         Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
         StringBuilder strBuilder = new StringBuilder();
         for (ConstraintViolation<?> violation : violations ) {
              strBuilder.append(violation.getMessage() + "\n");
         }
         
         GSILResponseMessage responseMessage = new GSILResponseMessage( GSILErrors.CONSTRAINT_VIOLATION.getCode(), e.getMessage(), strBuilder.toString(), false );
 		
         return new ResponseEntity<>(responseMessage, HttpStatus.BAD_REQUEST);
         
    }

}
