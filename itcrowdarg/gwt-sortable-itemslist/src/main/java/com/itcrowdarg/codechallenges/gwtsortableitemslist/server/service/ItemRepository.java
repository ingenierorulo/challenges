package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.domain.Item;

public interface ItemRepository extends JpaRepository<Item, Integer> {
    Page<Item> findAll(Pageable pageable);

    Item findFirstByText(String text);
    
    Item findOne(Integer id);
    
    Item findFirstByTextOrId(String text, Integer Id);

    List<Item> findByTextContainingIgnoreCase(String orElse, Pageable pageable);
    
    List<Item> findDistinctByTextIgnoreCase(String orElse, Pageable pageable); 
}
