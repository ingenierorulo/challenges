package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 
 */

public abstract class ItemBaseController {
	
	protected static Logger logger = Logger.getLogger( ItemBaseController.class );

	public ResponseEntity<Map<String, Object>> createResponse(boolean success, HttpStatus httpStatus, ItemResponseBody responseBody) {
		responseBody.addData( "success", success);
		return new ResponseEntity<Map<String,Object>>(responseBody.getBody(), httpStatus);
	}
}