package com.itcrowdarg.codechallenges.gwtsortableitemslist.client.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gwt.view.client.ProvidesKey;

/**
 * This class is used to represent an Item.
 *
 */

public class Item {
    
    @JsonIgnore
    public static final ProvidesKey<Item> KEY_PROVIDER = new ProvidesKey<Item>(){
        @Override
        public Object getKey(Item item) {
            return item == null ? null : item.getId();
        }
    };
    
    @JsonProperty("id")
    private Integer id;
    
    @JsonProperty("text")
    private String text;
    
    public Item() {
    }

    public Item(final String text) {
        this.text = text;
    }

    public Item(final int id, final String text) {
        this.id = id;
        this.text = text;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", text='" + text + '\'' +
                '}';
    }
}
