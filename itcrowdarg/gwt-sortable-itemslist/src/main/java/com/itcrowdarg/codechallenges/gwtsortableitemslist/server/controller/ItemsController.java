package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller;

import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller.vm.ItemVM;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller.vm.factory.ItemVMFactory;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.domain.Item;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.service.ItemService;

/**
 * 
 */
@RestController
@RequestMapping("/items")
public class ItemsController extends ItemBaseController{
	private final Logger logger = LoggerFactory.getLogger(ItemsController.class);
	@Autowired
	private ItemService itemService;

	/**
	 *
	 * e.g. curl -v http://localhost:8080/items?page=0
	 * 
	 * @param containingText
	 * @param page
	 * @return all items on database
	 */
	@GetMapping(produces = "application/json")
	public ResponseEntity<Map<String, Object>> getItems(String containingText, 
	        Pageable page){
		ItemResponseBody response = new ItemResponseBody();
		response.addData("items",  ItemVMFactory
		        .createItemListVM(itemService
		                .findByTextContainingIgnoreCase(Optional
		                        .ofNullable(containingText)
		                        .orElse(""), page)));
		logger.info("The items were successfully get.");
		return super.createResponse(Boolean.TRUE, HttpStatus.OK, response);
	}
	
	/**
	 * 
	 * @param item
	 * @return the Id of the new item
	 */
	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Map<String, Object>>  createItem(@RequestBody @Valid ItemVM item){
	    Item newItem = itemService.saveItem(item);
	    ItemResponseBody response = new ItemResponseBody();
	    response.addData("itemId", newItem.getId());
		return super.createResponse(Boolean.TRUE, HttpStatus.OK, response);
	}
	
	/**
	 * 
	 * @param itemId
	 * @param itemVM
	 * @return all info of the new item
	 */
	@PutMapping(value="/{itemId}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<Map<String, Object>>  changeItem(@PathVariable(value = "itemId") @Valid @Min(1) int itemId,
	        @RequestBody @Valid ItemVM itemVM){
	    ItemResponseBody response = new ItemResponseBody();
        response.addData("item", ItemVMFactory.createItemVM(itemService.updateItem(itemId, itemVM)));
        return super.createResponse(Boolean.TRUE, HttpStatus.OK, response);
	}
	
	/**
	 * 
	 * @param itemId
	 * @return success
	 */
	@DeleteMapping(value="/{itemId}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<Map<String, Object>> removeItem(@PathVariable(value = "itemId") @Valid @Min(1) int itemId) {
	    itemService.deleteItem(itemId);
        logger.info("Item deleted: " + itemId);
        return super.createResponse(Boolean.TRUE, HttpStatus.OK, new ItemResponseBody());
    }

	/**
	 * 
	 * @return success
	 */
	@DeleteMapping(produces = "application/json")
    public ResponseEntity<Map<String, Object>> reset() {
        itemService.deleteAll();
        logger.info("All items were successfully removed.");
        return super.createResponse(Boolean.TRUE, HttpStatus.OK, new ItemResponseBody());
    }
	
    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }
}
