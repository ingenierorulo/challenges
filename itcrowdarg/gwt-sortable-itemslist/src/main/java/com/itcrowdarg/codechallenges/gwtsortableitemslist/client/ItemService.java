package com.itcrowdarg.codechallenges.gwtsortableitemslist.client;

import java.util.Map;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import com.itcrowdarg.codechallenges.gwtsortableitemslist.client.domain.Item;



/**
 * This class is used as rest service for the {@link Item}.
 *
 */
@Singleton
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/items")
public interface ItemService extends RestService {

    /**
     * Get all Items from the server.
     *
     * @param text optional text to only get the Items which contain the given text
     * @param page get the page current number
     * @param size get size of the page
     * @param callback callback
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    void getItems(@QueryParam("containingText") final String text, 
                  @QueryParam("page") final int page, 
                  @QueryParam("size") final int size, 
                  MethodCallback<Map<String, Object>> callback);

    /**
     * Add a Item to the server.
     *
     * @param Item the Item to add
     * @param callback callback
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    void createItem(final Item item, 
                 final MethodCallback<Map<String, Object>> callback);

    /**
     * Update an Item and send to the server.
     * 
     * @param id the id of the item to update
     * @param Item the Item to update
     * @param callback callback
     */
    @PUT
    @Path("?id={id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    void changeItem(@PathParam("id") final Integer id, 
                    final Item item, 
                    final MethodCallback<Map<String, Object>> callback);
    
    /**
     * Delete a Item from the server.
     * 
     * @param Item the Item to delete
     * @param callback callback
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("?id={id}")
    void removeItem(@PathParam("id") final Integer id,
                    final MethodCallback<Map<String, Object>> callback);
    
    /**
     * Delete all items.
     *
     * @param callback callback
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    void reset(final MethodCallback<Map<String, Object>> callback);
}
