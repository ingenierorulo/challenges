package com.itcrowdarg.codechallenges.gwtsortableitemslist.client.ui.main;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.i18n.shared.AnyRtlDirectionEstimator;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.client.ItemService;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.client.domain.Item;

@Singleton
public class ItemsPanelView extends Composite {
    protected interface ItemPanelViewUiBinder extends UiBinder<HTMLPanel, ItemsPanelView> { }
    protected static final int REFRESH_INTERVAL = 1000; // ms
    protected static final int MIN_FILTER_TEXT = 3;
    protected static final int MAX_ALLOWED_TEXT = 25;
    protected static final String HISTORY_MAIN = "main";
    protected static Logger logger = Logger.getLogger(ItemsPanelView.class.getName());
    protected static ItemPanelViewUiBinder uiBinder = GWT.create(ItemPanelViewUiBinder.class);
    protected static final ItemService ITEMSERVICE = GWT.create(ItemService.class);
    protected ListDataProvider<Item> dataProviderList;
    protected ListHandler<Item> sortHandler;
    
    @UiField
    HorizontalPanel addPanel;
    
    @UiField
    Button clearButton;

    @UiField
    CellTable<Item> cellTable = new CellTable<>(Item.KEY_PROVIDER);
    
    @UiField
    SimplePager pager;

    @UiField
    TextBox itemTextBox;
    
    @UiField
    Button addItemButton;
    
    @UiField
    Label errorMsgLabel = new Label();
    
    @UiField
    Label lastUpdatedLabel = new Label();
    
    @Inject
    public ItemsPanelView() {
        initWidget(uiBinder.createAndBindUi(this));
        init();
    }

    /**
     * init all components of the ItemPanelView
     */
    private void init() {
        logger.info("ItemPanelView created...");
        initItemPanel();
        setupHistory();
        initListDataProvider();
        initSortHandler();
        initTableColumns();
        initErrorMsgLabel();
        initItemTextBox();
        addDataDisplay(cellTable);
        refreshItemsGrid();
    }

    /**
     * Add a display to the database. The current range of interest of the display
     * will be populated with data.
     * 
     * @param display a {@Link HasData}.
     */
    public void addDataDisplay(HasData<Item> display) {
        dataProviderList.addDataDisplay(display);
    }

    
    private void initErrorMsgLabel() {
        errorMsgLabel.setStyleName("errorMessage");
        errorMsgLabel.setVisible(false);
    }

    private void initItemPanel() {
        // Assemble Add Item panel.
        addPanel.add(itemTextBox);
        addItemButton.setEnabled(false);
        addPanel.add(addItemButton);
        addPanel.addStyleName("addPanel");
        
        itemTextBox.setFocus(true);
    }

    private void refreshingTimer() {
        // Setup timer to refresh list automatically.
        Timer refreshTimer = new Timer() {
          @Override
          public void run() {
             refreshItemsGrid();
          }
        };
        refreshTimer.scheduleRepeating(REFRESH_INTERVAL);
    }

    @UiHandler("addItemButton")  
    public void onAddItemButtonClick(ClickEvent event){
        addItem();
    }
    @UiHandler("itemTextBox")  
    public void onItemTextBoxKeyPress(KeyPressEvent event){
        if (event.getCharCode() == KeyCodes.KEY_ENTER) {
            addItem();
          }
    }
        
    /**
     * Add item to itemsGrid. Executed when the user clicks the addItemButton or
     * presses enter in the itemTextBox.
     */
    private void addItem() {
        
      final String itemText = SafeHtmlUtils.htmlEscape(itemTextBox.getText().trim());
      itemTextBox.setFocus(true);

      Item item = new Item(itemText);
      if (!isValid(item)){
          errorMsgLabel.setText("No valid text input!");
          itemTextBox.selectAll();
          return;
      }
      
      itemTextBox.setText("");
      
      // Add the stock to the table.
      int row = cellTable.getRowCount();
      dataProviderList.getList().add(item);
      
      Column<Item, String> removeItemButtonColumn = 
              new Column<Item, String>(new ActionCell("x", new ActionCell.Delegate<Item>() {

        @Override
        public void execute(Item object) {
            dataProviderList.getList().remove(item);
            deleteItem(item.getId());
        }
    })){

        @Override
        public String getValue(Item object) {
            return item.getText();
        }
      };
      cellTable.addColumn(removeItemButtonColumn);
      cellTable.redraw();
      addItem(item);
    }

    private void setupHistory() {
        History.newItem(HISTORY_MAIN);

        // Add history listener
        History.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                String token = event.getValue();
                if (!token.equals(HISTORY_MAIN)) {
                    History.newItem(HISTORY_MAIN);
                }
            }
        });
    }

    /**
     * fill the grid with all Items from the server.
     */
    public void refreshItemsGrid(){
        getItems("");
    }

    /**
     * get all Items from the server that contains a given text.
     * 
     * @param textFilter the text of the item
     * 
     */
    public void getItems(final String textFilter){
        logger.info("Getting items from server...");
        
        ITEMSERVICE.getItems(textFilter , pager.getPage(), pager.getPageSize(), new MethodCallback<Map<String, Object>>() {
            
            @Override
            public void onSuccess(Method method, Map<String, Object> response) {
                dataProviderList.getList().addAll((List<Item>) response.get("items"));
                updateLastRefresh();
                logger.info("All items were successfully get.");
            }
            
            @Override
            public void onFailure(Method method, Throwable exception) {
                updateOnFail("Error when trying to delete all items on server", exception);
            }
        });
    }
    
    /**
     * Send a new item to the server. On success refresh the itemsGrid.
     *
     * @param text the text of the item
     */
    private void addItem(final Item item) {
        logger.info("saving item"+ item.getText() + " on server...");
        ITEMSERVICE.createItem(item, new MethodCallback<Map<String, Object>>() {

            @Override
            public void onFailure(Method method, Throwable exception) {
                updateOnFail("Error when trying to save the item on server: ", exception);
            }

            @Override
            public void onSuccess(Method method, Map<String, Object> response) {
//                dataProviderList.getList().add(index,item);
                updateLastRefresh();
                logger.info("The item [" + item.getText() + "] was successfully added.");
            }
        });
    }
    
    /**
     * Send the item modified to the server. On success refresh the itemsGrid.
     * 
     * @param index position of the item 
     * @param item item modified
     */
    private void updateItem(final int index, final Item item) {
        if (isValid(item)){
            logger.info("updating item...");
            ITEMSERVICE.changeItem(item.getId(), item, new MethodCallback<Map<String, Object>>(){

                @Override
                public void onFailure(Method method, Throwable exception) {
                    updateOnFail("Error when trying to update the item with id [" + item.getId() + "] on server", exception);
                }

                @Override
                public void onSuccess(Method method, Map<String, Object> response) {
                    if (item.getId() >= 0 ){
                        dataProviderList.getList().set(index, (Item) response.get("item"));
                        logger.info("The item unmber [" + index + "] was successfully updated.");
                    }
                    updateLastRefresh();                       
                }
                
            });
        }
    }
    
    private void deleteItem(final int itemId) {
        if (itemId <= 0){
            return;
        }
        logger.info("deleteting item...");
        ITEMSERVICE.removeItem(itemId, new MethodCallback<Map<String, Object>>(){

            @Override
            public void onFailure(Method method, Throwable exception) {
                updateOnFail("Error when trying to delte the item with id [" + itemId + "] on server", exception);
            }

            @Override
            public void onSuccess(Method method, Map<String, Object> response) {
                updateLastRefresh();
                logger.info("The item [" + itemId + "] was successfully removed.");
            }
            
        });
    }
    /**
     * clear the database
     */
    private void clear() {
        logger.info("cleaning...");
        ITEMSERVICE.reset(new MethodCallback<Map<String, Object>>() {

            @Override
            public void onFailure(Method method, Throwable exception) {
                updateOnFail("Error when trying to delete all items on server: ", exception);
            }

            @Override
            public void onSuccess(Method method, Map<String, Object> response) {
                updateLastRefresh();
                logger.info("The items were successfully removed.");
            }
        });
    }
    

    private boolean textExists(String text) {
        return dataProviderList.getList().stream()
                .map(Item::getText)
                .anyMatch(text::equalsIgnoreCase);
    }  
    
    private boolean isValid(Item item){
        if (null == item 
                || (null == item.getText())
                || "".equalsIgnoreCase(item.getText().trim())
                || item.getText().trim().length() > MAX_ALLOWED_TEXT
                || textExists(item.getText()))
            return false;
        return true;
    }
    
    private void initSortHandler() {
        // Attach a column sort handler to the ListDataProvider to sort the
        // list.
        sortHandler = new ListHandler<Item>(dataProviderList.getList());
        cellTable.addColumnSortHandler(sortHandler);
    }

    private void initItemTextBox() {
        itemTextBox.ensureDebugId("itemTextBox");
        itemTextBox.setDirectionEstimator(AnyRtlDirectionEstimator.get());
        itemTextBox.setMaxLength(MAX_ALLOWED_TEXT);
        itemTextBox.addKeyUpHandler(new KeyUpHandler() {
            
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (itemTextBox.getVisibleLength() > 0 ){
                    addItemButton.setEnabled(true);
                    dataProviderList.getList().addAll(dataProviderList.getList()
                            .stream()
                            .filter(x -> itemTextBox.getText().contains(x.getText()))
                            .collect(Collectors.toList()));
                    
                }
                else {
                    addItemButton.setEnabled(false);
                }
                
                
                
            }
        });
    }
    
    /**
     * Display timestamp showing last refresh.
     */
    private void updateLastRefresh(){
        lastUpdatedLabel.setText("Last update : "
            + DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_MEDIUM).format(new Date()));
     // Clear any errors.
        cellTable.redraw();
        errorMsgLabel.setVisible(false);
    }
    
    private void updateOnFail(String errorMessage, Throwable exception){
        logger.warning(errorMessage + (null != exception ? exception : ""));
        errorMsgLabel.setText(errorMessage);
        errorMsgLabel.setVisible(true);
    }
    

    @UiHandler("clearButton")
    public void onButtonClick(final ClickEvent event) {
        logger.info("Click Detected by GWT UiBinder");
        dataProviderList.getList().clear();
        clear();
    }

    private void initTableColumns() {
        cellTable.setWidth("100%");
        cellTable.setAutoHeaderRefreshDisabled(true);
        
        // ------------------Item id number------------------------------------------
        Column<Item, String> itemId = new Column<Item, String>(new TextCell()) {
          @Override
          public String getValue(Item object) {
            return Integer.toString(object.getId());
          }
        };
        itemId.setSortable(true);
        itemId.setDefaultSortAscending(false);
        sortHandler.setComparator(itemId, new Comparator<Item>() {
          @Override
          public int compare(Item o1, Item o2) {
              if (null == o1 || null == o1.getId()){
                  if (null == o2 || null == o2.getId()){
                      return 0;
                  }
                  return -1;
              }
              if(null == o2 || null == o2.getId()){
                  return 1;
              }
              return Integer.compare(o1.getId(), o2.getId());
          }
        });
        cellTable.addColumn(itemId, "Item Id");
        cellTable.setColumnWidth(itemId, 7, Unit.PCT);
        
        // ------------------------Text.-----------------------------------------------
        Column<Item, String> textColumn = new Column<Item, String>(new EditTextCell()) {
            @Override
            public String getValue(Item object) {
                return object.getText();
            }
        };
        textColumn.setSortable(true);
        textColumn.setDefaultSortAscending(false);
        sortHandler.setComparator(textColumn, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                return o1.getText().compareTo(o2.getText());
            }
        });
        textColumn.setFieldUpdater(new FieldUpdater<Item, String>() {
            @Override
            public void update(int index, Item object, String value) {
                if (!isValid(object)){
                    // We only modified the cell, so do a local redraw.
                    cellTable.redraw();
                }
                else {
                 // Called when the user changes the value.
                    object.setText(value);
                    updateItem(index, object);
                }
            }
        });
        cellTable.addColumn(textColumn, "Text");
        cellTable.setColumnWidth(textColumn, 40, Style.Unit.PCT);
    }

    private void initListDataProvider() {
        dataProviderList = new ListDataProvider<Item>(new ArrayList<Item>());
        // Create a Pager to control the table.
        SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
        pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
        pager.setDisplay(cellTable);
        // Set the message to display when the table is empty.
        cellTable.setEmptyTableWidget(new Label("No Data"));
    }

    public static ItemPanelViewUiBinder getUiBinder() {
        return uiBinder;
    }

    public static void setUiBinder(ItemPanelViewUiBinder uiBinder) {
        ItemsPanelView.uiBinder = uiBinder;
    }

    public ListDataProvider<Item> getDataProviderList() {
        return dataProviderList;
    }

    public void setDataProviderList(ListDataProvider<Item> dataProviderList) {
        this.dataProviderList = dataProviderList;
    }

    public ListHandler<Item> getSortHandler() {
        return sortHandler;
    }

    public void setSortHandler(ListHandler<Item> sortHandler) {
        this.sortHandler = sortHandler;
    }

    public HorizontalPanel getAddPanel() {
        return addPanel;
    }

    public void setAddPanel(HorizontalPanel addPanel) {
        this.addPanel = addPanel;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public void setClearButton(Button clearButton) {
        this.clearButton = clearButton;
    }

    public CellTable<Item> getItemsGrid() {
        return cellTable;
    }

    public void setItemsGrid(CellTable<Item> cellTable) {
        this.cellTable = cellTable;
    }

    public SimplePager getPager() {
        return pager;
    }

    public void setPager(SimplePager pager) {
        this.pager = pager;
    }

    public TextBox getItemTextBox() {
        return itemTextBox;
    }

    public void setItemTextBox(TextBox itemTextBox) {
        this.itemTextBox = itemTextBox;
    }

    public Button getAddItemButton() {
        return addItemButton;
    }

    public void setAddItemButton(Button addItemButton) {
        this.addItemButton = addItemButton;
    }

    public Label getErrorMsgLabel() {
        return errorMsgLabel;
    }

    public void setErrorMsgLabel(Label errorMsgLabel) {
        this.errorMsgLabel = errorMsgLabel;
    }

    public Label getLastUpdatedLabel() {
        return lastUpdatedLabel;
    }

    public void setLastUpdatedLabel(Label lastUpdatedLabel) {
        this.lastUpdatedLabel = lastUpdatedLabel;
    }
}
