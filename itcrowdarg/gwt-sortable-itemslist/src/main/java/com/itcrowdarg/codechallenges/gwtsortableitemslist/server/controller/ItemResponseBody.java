package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller;

import java.util.HashMap;
import java.util.Map;

public class ItemResponseBody {
	private Map<String, Object> body;
	
	public ItemResponseBody(){
		this.body = new HashMap<String, Object>();
	}

	public Map<String, Object> getBody() {
		return body;
	}

	public void setBody(Map<String, Object> body) {
		this.body = body;
	}
	
	public void addData(String key, Object data){
		body.put(key, data);
	}
}
