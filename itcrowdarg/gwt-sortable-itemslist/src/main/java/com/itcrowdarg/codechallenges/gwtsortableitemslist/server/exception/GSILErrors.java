package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception;

public enum GSILErrors {
	CONSTRAINT_VIOLATION("0002","There has been an error in the parameter values"),
	INTERNAL_ERROR("0003", "Internal error, please start your operation again."),
	PERSISTENCE_ERROR("0005", "There has been an error on the database!"),
	SERVICE_ERROR("0006", "There has been an error executing a service."),
	MISSING_SERVICE_DATA("0009", "No data was provided to execute the service."),
	RESOURCE_NOT_FOUND("0025", "Could not get the designated record from the repository or no record was found for the given input."),
	INVALID_INPUT("0026", "The given input is not in a valid format."),
	;
	private String code;

	private String message;

	private GSILErrors(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
