package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception;

public class GSILServiceException extends GSILBaseException {

	/** Serial Version */
	private static final long serialVersionUID = -6947374016044123377L;

	public GSILServiceException(GSILErrors errorCode) {
		super(errorCode);
	}

	public GSILServiceException(String message, GSILErrors errorCode) {
		super(message, errorCode);
	}

	public GSILServiceException(String message, Throwable cause,
			GSILErrors errorCode) {
		super(message, cause, errorCode);
	}

	public GSILServiceException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace,
			GSILErrors errorCode) {
		super(message, cause, enableSuppression, writableStackTrace, errorCode);
	}

	public GSILServiceException(Throwable cause, GSILErrors errorCode) {
		super(cause, errorCode);
	}
}
