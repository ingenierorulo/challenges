package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller.vm.factory;

import java.util.List;
import java.util.stream.Collectors;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller.vm.ItemVM;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.domain.Item;

public class ItemVMFactory {
       
    public static ItemVM createItemVM(Item item){
        return null != item ? new ItemVM(item.getId(),item.getText()) : new ItemVM();
    }
    
    public static List<ItemVM> createItemListVM(List<Item> items){
        return items.stream()
                    .map(temp -> {
                        ItemVM obj = new ItemVM();
                        obj.setId(temp.getId());
                        obj.setText(temp.getText());
                        return obj;
                    })
                    .collect(Collectors.toList());
    }
}
