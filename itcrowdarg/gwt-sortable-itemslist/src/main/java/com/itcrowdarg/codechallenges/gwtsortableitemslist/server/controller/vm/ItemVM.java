package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller.vm;

import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

public class ItemVM {
    private Integer id;
    
    @NotBlank
    @Size(min=1, max=25) 
    private String text;
    
    public ItemVM() {
    }
 
    public ItemVM(String text) {
        super();
        this.text = text;
    }

    public ItemVM(Integer id, String text) {
        super();
        this.id = id;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
}
