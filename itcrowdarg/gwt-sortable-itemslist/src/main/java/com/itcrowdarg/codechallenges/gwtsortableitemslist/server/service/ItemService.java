package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller.vm.ItemVM;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.domain.Item;

public interface ItemService {
    Page<Item> findAll(Pageable pageable);

    Item findFirstByText(String text);

    Item findOne(int itemId);
    
    Item updateItem(int itemId, ItemVM itemVM);

    Item saveItem(ItemVM item);
    
    void deleteItem(int itemId);
    
    void deleteAll();

    List<Item> findByTextContainingIgnoreCase(String orElse, Pageable pageable);
    
    List<Item> findDistinctByTextIgnoreCase(String orElse, Pageable pageable);
}
