package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.handler.exception;

public class GSILResponseMessage {

	private String errorCode;
	private String errorDescription;
	private String errorMessage;
	private boolean success;
	
	public GSILResponseMessage() { }

	public GSILResponseMessage( String errorCode, String errorDescription, String errorMessage, boolean success ) {
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
		this.errorMessage = errorMessage;
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setErrorCode( String errorCode ) {
		this.errorCode = errorCode;
	}

	public void setErrorDescription( String errorDescription ) {
		this.errorDescription = errorDescription;
	}

	public void setErrorMessage( String errorMessage ) {
		this.errorMessage = errorMessage;
	}

	public void setSuccess( boolean success ) {
		this.success = success;
	}

}
