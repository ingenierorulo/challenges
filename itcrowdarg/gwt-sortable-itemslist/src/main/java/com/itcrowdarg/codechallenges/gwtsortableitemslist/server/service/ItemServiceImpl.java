package com.itcrowdarg.codechallenges.gwtsortableitemslist.server.service;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.controller.vm.ItemVM;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.domain.Item;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception.GSILErrors;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception.GSILPersistenceException;
import com.itcrowdarg.codechallenges.gwtsortableitemslist.server.exception.GSILServiceException;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {
    private static final Logger logger = Logger.getLogger(ItemServiceImpl.class);
    
    @Autowired
    private ItemRepository itemRepository;
    
    @Override
    public Item findOne(int itemId) {
        try {
            return itemRepository.findOne(itemId);
        } catch (GSILPersistenceException e) {
            logger.error(e.getErrorCode().getMessage(), e);
            throw new GSILServiceException("There was an error when finding the item " + itemId,
                    e, e.getErrorCode());
        }
    }
    
    @Override
    public Page<Item> findAll(Pageable pageable) {
        try {
            return itemRepository.findAll(pageable);
        } catch (GSILPersistenceException e) {
            logger.error(e.getErrorCode().getMessage(), e);
            throw new GSILServiceException("There was an error when getting all saved items.",
                    e, e.getErrorCode());
        }
    }

    @Override
    public Item findFirstByText(String text) {
        try {
            return itemRepository.findFirstByText(text);
        } catch (GSILPersistenceException e) {
            logger.error(e.getErrorCode().getMessage(), e);
            throw new GSILServiceException("There was an error when getting the item " + text,
                    e, e.getErrorCode());
        }
    }

    @Override
    public Item updateItem(int itemId, ItemVM itemVM) {
        Item item = null;
        try {
            if (1 > itemId || null == itemVM || StringUtils.isEmpty(itemVM.getText())){
                logger.error("The item could not be updated. No valid text or Id");
                throw new GSILServiceException("The item could not be updated. No valid text or Id", GSILErrors.SERVICE_ERROR);  
            }
            item = itemRepository.findOne(itemId);
            if (null == item){
                logger.error("The item could not be updated. No valid Id");
                throw new GSILServiceException("The item could not be updated. No valid Id", GSILErrors.SERVICE_ERROR);
            }
            item.setText(itemVM.getText());
            return itemRepository.save(item);
        } catch (GSILPersistenceException e) {
            logger.error("The item could not be created", e);
            throw new GSILServiceException("The item could not be created",
                    e, e.getErrorCode());
        }
    }

    @Override
    public Item saveItem(ItemVM item) {
        try {
            if (null != item && null != itemRepository.findFirstByTextOrId(item.getText(),item.getId())){
                logger.error("The item could not be saved. Item duplicated");
                throw new GSILServiceException("The item could not be saved. Item duplicated", GSILErrors.SERVICE_ERROR);
            }
            return itemRepository.save(new Item(item.getId(),item.getText()));
        } catch (GSILPersistenceException e) {
            logger.error("The item could not be saved", e);
            throw new GSILServiceException("The item could not be saved",
                    e, e.getErrorCode());
        }
    }

    @Override
    public List<Item> findByTextContainingIgnoreCase(String orElse, Pageable pageable) {
        try {
            return itemRepository.findByTextContainingIgnoreCase(orElse, pageable);
        } catch (GSILPersistenceException e) {
            logger.error("The item could not be found", e);
            throw new GSILServiceException("The item could not be found",
                    e, e.getErrorCode());
        }
    }

    @Override
    public List<Item> findDistinctByTextIgnoreCase(String orElse, Pageable pageable) {
        try {
            return itemRepository.findDistinctByTextIgnoreCase(orElse, pageable);
        } catch (GSILPersistenceException e) {
            logger.error("There was an error when trying to get the item " + orElse, e);
            throw new GSILServiceException("There was an error when trying to get the item " + orElse,
                    e, e.getErrorCode());
        }
    }

    @Override
    public void deleteItem(int itemId) {
        try {
            if(1 > itemId){
                logger.error("The item could not be deleted. No valid Id");
                throw new GSILServiceException("The item could not be deleted. No valid Id", GSILErrors.SERVICE_ERROR);
            }
            itemRepository.delete(itemId);
        } catch (GSILPersistenceException e) {
            logger.error("The item could not be deleted", e);
            throw new GSILServiceException("The item could not be saved",
                    e, e.getErrorCode());
        }
    }
    
    @Override
    public void deleteAll() {
        try {
            itemRepository.deleteAll();
        } catch (GSILPersistenceException e) {
            logger.error("Unable to remove all items of the database.", e);
            throw new GSILServiceException("Unable to remove all items of the database.",
                    e, e.getErrorCode());
        }
    }
    
    public ItemRepository getItemRepository() {
        return itemRepository;
    }

    public void setItemRepository(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

   

    

}
